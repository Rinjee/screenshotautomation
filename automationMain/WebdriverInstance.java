package automationMain;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebdriverInstance {
	public static WebDriver driver;
	
	public static void create_instance() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\eclipse_workshop\\TIJ_Automation\\driver\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}
}
