package automationMain;
import java.io.IOException;

import org.testng.annotations.Test;

public class AutomationMain {

	@Test(priority=1)
	public void runWebdriverInstance() throws InterruptedException {
		WebdriverInstance.create_instance();
		
	}
	@Test(priority=2)
	public void runScreenshotAuto() throws InterruptedException, IOException {
		ScreenshotAutomation.main();
	}
}
