package automationMain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class ScreenshotAutomation extends WebdriverInstance {

	public static void main() throws InterruptedException, IOException {
		// Gets the current date and time for start time
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		
		//output on >txt
		File file = new File("C:\\Users\\user\\eclipse_workshop\\TIJ_Automation\\file\\sample.txt");
		FileOutputStream fos = new FileOutputStream(file);
		//Instantiating the PrintStream class
		PrintStream ps = new PrintStream(fos);


					   driver.get("http://www.growba.tokyo/redmine/login");
				        Thread.sleep(2000);
				        driver.findElement(By.xpath("//*[@id=\'username\']")).sendKeys("rinchendorji");
				        
				        Thread.sleep(500);
				        driver.findElement(By.xpath("//*[@id=\'password\']")).sendKeys("Rinchen0076*");
				        
				        Thread.sleep(500);
				        driver.findElement(By.xpath("//*[@id=\'login-submit\']")).click();
				        
				        //taking screenshot and saving it with a date as it's name
				        String FileName = myDateObj.toString().replace("-", "/") + ".png";
				        File scr = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				        File dest = new File("C:/Users/user/eclipse_workshop/TIJ_Automation/screenshot/"+FileName);
				        FileUtils.copyFile(scr, dest);
				        Thread.sleep(500);
				       
				     driver.close();
				   
				        LocalDateTime myDateObj2 = LocalDateTime.now();
				        DateTimeFormatter myFormatObj2 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
				        
				        
				        //output on console
				        System.out.println(" Start Time is "+myFormatObj.format(myDateObj));
				        System.out.println(" Finish Time is " +myFormatObj2.format(myDateObj2));
				       //Text output for direction for the user
				        System.out.println("This time is also Printed on this path: ");
				        System.out.println( file.getAbsolutePath());
				        System.out.println( "Text File name is \"sample\"");
				        
				        //output on .txt file
				        System.setOut(ps);
				        System.out.println("Time taken for the code");
				        System.out.println(" Start Time is "+myFormatObj.format(myDateObj));
				        System.out.println(" Finish Time is " +myFormatObj2.format(myDateObj));
				 
				    
	}

}
